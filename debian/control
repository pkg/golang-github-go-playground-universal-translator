Source: golang-github-go-playground-universal-translator
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Cyril Brulebois <cyril@debamax.com>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-go-playground-locales-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-go-playground-universal-translator
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-go-playground-universal-translator.git
Homepage: https://github.com/go-playground/universal-translator
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/go-playground/universal-translator

Package: golang-github-go-playground-universal-translator-dev
Architecture: all
Depends: golang-github-go-playground-locales-dev,
         ${misc:Depends}
Description: translator for Go using CLDR data and pluralization rules
 Universal Translator is an i18n Translator for Go using CLDR data and
 pluralization rules. This package is a thin wrapper around its
 locales counterpart (golang-github-go-playground-locales-dev) in
 order to store and translate text for use in your applications.
 .
 Features:
  - Rules generated from the CLDR data
  - Contains Cardinal, Ordinal and Range Plural Rules
  - Contains Month, Weekday and Timezone translations
  - Contains Date & Time formatting functions
  - Contains Number, Currency, Accounting and Percent formatting
    functions
  - Supports the Gregorian calendar only
  - Supports loading translations from files
  - Supports exporting translations to files
